import boto3
import base64
from botocore.exceptions import ClientError

from flask_login import UserMixin

# AWS Keys
ACCESS_KEY_ID = 'AKIAJKRU65VZQTR6D4NA'
SECRET_ACCES_KEY = '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'

# Setting up DynamoDB and key management system.
dynamoDBClient = boto3.client('dynamodb', region_name='us-east-2',\
    aws_access_key_id=ACCESS_KEY_ID,\
    aws_secret_access_key= SECRET_ACCES_KEY)
dynamoDBResource = boto3.resource('dynamodb', region_name='us-east-2',\
    aws_access_key_id=ACCESS_KEY_ID,\
    aws_secret_access_key= SECRET_ACCES_KEY)
session = boto3.session.Session()
kms = session.client('kms', region_name='us-east-2',\
    aws_access_key_id=ACCESS_KEY_ID,\
    aws_secret_access_key= SECRET_ACCES_KEY)

# Contains username. Used for session management.
class User(UserMixin):
    def __init__(self, username):
        self.id = username

# Class used to interact with the DynamoDB table containing user information.
class UserController:
    def __init__(self):
        self.table = dynamoDBResource.Table('user_profiles')

    # Creates a new user profile if the username is not in use.
    def create_user(self, username, email, password, root_uid):
        try:
            encrypted_password = self.encrypt_password(password)
            response = self.table.put_item(
                Item = {
                    'username': username,
                    'email': email,
                    'password': encrypted_password,
                    'root_uid': root_uid
                },
                ConditionExpression='attribute_not_exists(username)'
            )
            return response['ResponseMetadata']
        except dynamoDBClient.exceptions.ConditionalCheckFailedException as e:
            raise(e)

    # Checks if the password entered is correct. Decrypts the password stored in the server
    # and compares it to the one entered by the client.
    def authenticate(self, username, password):
        response = self.table.get_item(Key={'username': username})
        if 'Item' in response:
            user_profile = response['Item']
            stored_password = self.decrypt_password(user_profile['password'])
            return password == stored_password
        else:
            return False

    # Encrypts a password using AWS's KMS
    def encrypt_password(self, password):
        key_id = 'alias/PasswordKeyAlias'
        ciphertext = kms.encrypt(KeyId=key_id, Plaintext=password)
        encrypted_binary = ciphertext["CiphertextBlob"]
        return base64.b64encode(encrypted_binary).decode()

    # Decrypts an encripted stored password.
    def decrypt_password(self, encripted_password):
        binary_data = base64.b64decode(encripted_password)
        meta = kms.decrypt(CiphertextBlob=binary_data)
        return meta['Plaintext'].decode()

    # Returns the uid of a user's root directoryt in the filesystem.
    def get_user_root_uid(self, username):
        response = self.table.get_item(Key={'username': username})
        return response['Item']['root_uid']
