import boto3
from boto3.dynamodb.conditions import Key, Attr
import os

# AWS keys and S3 bucket name
ACCESS_KEY_ID = 'AKIAJKRU65VZQTR6D4NA'
SECRET_ACCES_KEY = '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'
BUCKET = 'beanstalk-cloud-computing-project-bucket-01'

# Constant Directory UIDs
ROOT_UID = "1"
USERS_UID = "2"

# Class that encapsulates all information for a directory.
class Directory:
    def __init__(self, name, date, owner, uid, parent=USERS_UID):
        self.name = name
        self.date = date
        self.parent = parent
        self.owner = owner
        self.uid = uid
        self.is_directory = True

# Class that encapsulates all information for a file.
class File:
    def __init__(self, name, date, parent, size, file_type, owner, uid, file_object=None):
        self.name = name
        self.date = date
        self.parent = parent
        self.size = size
        self.file_type = file_type if not file_type == '' else 'Unknown'
        self.owner = owner
        self.file_object = file_object
        self.uid = uid
        self.is_directory = False

    def __repr__(self):
        return 'File [\n\tName: {}\n\tDate: {}\n\tParent: {}\n\tOwner: {}\n\tSize: {}\n\tType: {}\n\tUID: {}\n]'\
            .format(self.name, self.date, self.parent, self.owner, self.size, self.file_type, self.uid)

# Class exclusively used by the server to interact with the filesystem. (DynamoDB and S3)
class FileSystemController:
    def __init__(self):
        # Setting up DynamoDB
        self.table_name = 'filesystemV2'
        self.dynamodb = boto3.resource('dynamodb', region_name='us-east-2',\
            aws_access_key_id=ACCESS_KEY_ID,\
            aws_secret_access_key= SECRET_ACCES_KEY)
        self.table = self.dynamodb.Table(self.table_name)

        # Setting up S3
        self.bucket_name = 'beanstalk-cloud-computing-project-bucket-01'
        self.s3 = boto3.resource('s3',\
            aws_access_key_id=ACCESS_KEY_ID,\
            aws_secret_access_key= SECRET_ACCES_KEY)
        self.bucket = self.s3.Bucket(self.bucket_name)

        self.s3_client = boto3.client('s3', region_name='us-east-2',\
            aws_access_key_id=ACCESS_KEY_ID,\
            aws_secret_access_key= SECRET_ACCES_KEY)

    # Given a string path, it crawls the database and gathers the uid's of each
    # level in the path.
    def crawl_path(self, root_uid, path):
        if len(path) == 0:
            return [root_uid]

        next_dir_name = path[0]
        response = self.table.query(
            KeyConditionExpression=Key('parent_uid').eq(root_uid),
            FilterExpression=Attr('name').eq(next_dir_name) & Attr('is_directory').eq(True)
        )

        items = response['Items']
        if len(items) == 1:
            next_directory = items[0]['uid']
            other_directories = self.crawl_path(next_directory, path[1:])

            if other_directories:
                return [next_directory] + other_directories if not other_directories[0] == next_directory else [next_directory]
            else:
                return None
        else:
            return None

    # Uploads a file-like object to S3 and updates the database.
    def upload_file(self, file):
        # Uploads to S3
        self.bucket.upload_fileobj(file.file_object, file.uid)
        # Adds file to the 'filesystem' in DynamoDB
        self.put_file(file)

    # Adds a file entry in the database.
    def put_file(self, file):
        if not self.directory_exists(file.parent):
            return None
        else:
            response = self.table.put_item(
                Item = {
                    'parent_uid': file.parent,
                    'uid': file.uid,
                    'name': file.name,
                    'size': file.size,
                    'date': file.date,
                    'type': file.file_type,
                    'owner': file.owner,
                    'is_directory': False
                }
            )
            return response['ResponseMetadata']

    # Adds a directory entry in the database.
    def put_directory(self, directory):
        if not self.directory_exists(directory.parent):
            return None
        else:
            response = self.table.put_item(
                Item = {
                    'parent_uid': directory.parent,
                    'uid': directory.uid,
                    'name': directory.name,
                    'date': directory.date,
                    'type': 'Directory',
                    'owner': directory.owner,
                    'is_directory': True,
                    'size': '--'
                }
            )
            return response['ResponseMetadata']

    # Gets an entry from the database with a given uid (and an optional parent uid) and returns it's information.
    def get_item(self, uid, parent_uid=None):
        if not parent_uid:
            response = self.table.query(
                IndexName='uid-index',
                KeyConditionExpression=Key('uid').eq(uid)
            )
            if len(response['Items']) > 0:
                item = response['Items'][0]
            else:
                return None
        else:
            response = self.table.get_item(
                Key={'parent_uid': parent_uid, 'uid': uid}
            )
            if 'Item' in response:
                item = response['Item']
            else:
                return None

        if item['is_directory']:
            return Directory(\
                name=item['name'],\
                date=item['date'],\
                owner=item['owner'],\
                uid=item['uid'],\
                parent=item['parent_uid'])
        else:
            return File(\
                name=item['name'],\
                date=item['date'],\
                parent=item['parent_uid'],\
                size=item['size'],\
                file_type=item['type'],\
                owner=item['owner'],\
                uid=item['uid']
            )

    # Wrapper for the get_item method.
    def get_directory(self, directory_uid, parent_uid=None):
        return self.get_item(directory_uid, parent_uid)

    # Returns whether a directory with a given uid exists already.
    def directory_exists(self, directory_uid):
        response = self.get_directory(directory_uid)
        return not response == None

    # Gets all items within a directory by filtering entries in the database
    # that share a common parent uid. Returns informartion on all the contents and the directory itself.
    def get_directory_contents(self, directory_uid):
        directory = self.get_directory(directory_uid)
        # finding root directory in file dynamo
        items_found = self.table.query(
            KeyConditionExpression=Key('parent_uid').eq(directory_uid)
        )

        contents = []
        for item in items_found['Items']:
            contents.append({
                'date': item['date'],
                'size': item['size'],
                'type': item['type'],
                'is_directory': item['is_directory'],
                'name': item['name'],
                'uid': item['uid']
            })
        return contents, directory

    # CHecks whether an item with a given name exists in a directory to avoid collsiions.
    def item_exists_in_directory(self, name, parent):
        items_found = self.table.query(
            KeyConditionExpression=Key('parent_uid').eq(parent),
            FilterExpression=Attr('name').eq(name)
        )

        for item in items_found['Items']:
            if item['name'] == name:
                return item
        return None

    # Updates an entry in the database by changing its name attribute.
    def rename_item(self, uid, parent_uid, new_name):
        response = self.table.update_item(
            Key={
                'parent_uid': parent_uid,
                'uid': uid
            },
            UpdateExpression="SET #n = :n",
            ExpressionAttributeValues={
                ':n': new_name
            },
            ExpressionAttributeNames={
                "#n": "name"
            }
        )

    # Deletes an existing item from the database and creates a new entry with
    # the same information but an updated parent uid.
    def change_parent(self, uid, parent_uid, new_parent_uid):
        item = self.get_item(uid, parent_uid)

        response = self.table.delete_item(
            Key={'parent_uid': item.parent, 'uid': item.uid}
        )

        item.parent = new_parent_uid
        if item.is_directory:
            self.put_directory(item)
        else:
            self.put_file(item)

    #------------ Methods for deleting items-------------

    # Deletes an item with a given uid from S3 and Dynamo.
    def delete_item(self, uid):
        item = self.get_item(uid)
        if item.is_directory == False:
            self.s3_client.delete_object(Bucket=BUCKET, Key=uid)
            response = self.table.delete_item(
                Key={'parent_uid': item.parent, 'uid': item.uid}
            )
        return 0

    # Deletes an empty directory.
    def delete_directory(self, uid):
        if (self.is_directory_empty(uid) == True):
            item = self.get_directory(uid)
            response = self.table.delete_item(
                Key={'parent_uid': item.parent, 'uid': item.uid}
            )
            return 0
        else:
            return 1

    # Checks if a directory has any children.
    def is_directory_empty(self, uid):
        #item = self.get_directory(uid)
        contents, _ = self.get_directory_contents(uid)
        if len(contents) > 0:
            return False
        else:
            return True

    # Recursively deletes the contents of a directory.
    def handle_directory_delete(self, uid):
        #item = self.get_directory(uid)
        contents, _ = self.get_directory_contents(uid)
        for i in contents:
            if i['is_directory'] == False:
                self.delete_item(i['uid'])
            elif i['is_directory'] == True:
                if (self.is_directory_empty(i['uid']) == True):
                    self.delete_directory(i['uid'])
                else:
                    self.handle_directory_delete(i['uid'])
        if (self.is_directory_empty(uid) == True):
            self.delete_directory(uid)

    # ------------------------------------------

    # Downloads all the items passed in the uids list from S3 to a temporal directory
    # in the server. It does so in a recursive manner to mirror the file hierarchy
    # from the filesystem.
    def download_items(self, uids, path):
        for uid in uids:
            item = self.get_item(uid)
            item_path = os.path.join(path, item.name)
            if item.is_directory:
                os.mkdir(item_path)
                contents, _ = self.get_directory_contents(uid)
                uids = [c['uid'] for c in contents]
                self.download_items(uids, item_path)
            else:
                self.bucket.download_file(uid, item_path)




