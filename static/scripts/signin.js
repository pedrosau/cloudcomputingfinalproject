var accountCreator = {
  onReady: function() {
    accountCreator.username = $("#newUsername");
    accountCreator.email = $("#newEmail");
    accountCreator.password = $("#newPassword");
    accountCreator.confirmPasword = $("#confirmNewPassword");
    accountCreator.signUpMessage = $("#signUpMessage");

    $("#signup").click(accountCreator.signUp);
  },

  signUp: function() {
    if (accountCreator.validInfo()) accountCreator.postSignUpForm();
  },

  validInfo: function() {
    var passwordsMatch = this.password.val() === this.confirmPasword.val();
    var noEmptyFields = (this.username.val().length > 0 && this.email.val().length > 0
      && this.password.val().length > 0 && this.confirmPasword.val().length > 0);

    if (!passwordsMatch) this.signUpMessage.text("Password fields do not match.");
    return passwordsMatch && noEmptyFields;
  },

  postSignUpForm: function() {
    $.post( "/signup", $("#createAccountForm").serialize(),
            function(data) {
              $("#signupModal").modal('hide');
            }
          )
          .error(function(xhr) {
            switch(xhr.status) {
              case 409:
                accountCreator.signUpMessage.text("Username already exists.");
                break;
              default:
                accountCreator.signUpMessage.text("There was an error. Please try again.");
            }
          });
  }
}

$(document).ready(accountCreator.onReady);