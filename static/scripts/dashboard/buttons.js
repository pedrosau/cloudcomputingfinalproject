// **** Gets selected rows ******
function getSelected() {
  var checkboxes = document.querySelectorAll("input[type='checkbox']");
  var selected = [];
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) selected.push(checkbox);
  });
  return selected;
}

// ****** Back Button ********
var backButton = document.getElementById("backButton");
backButton.onclick = function() {
  $.post("/update_directory", {directory_uid: directoryInfo.parent}, getFiles);
}

// ****** Rename Button ************
var renameButton = document.getElementById("renameButton");
renameButton.onclick = function() {
  var selected = getSelected();
  if (selected.length != 1) return;
  $('#renameModal').modal('show');
}

var submitRename = document.getElementById("submitRenameItem");
submitRename.onclick = function() {
  var selectedItem = getSelected()[0];
  var uid = selectedItem.parentNode.parentNode.id;
  var newName = document.getElementById('newName');
  if (newName.value.length == 0) return false;

  $.post("/rename_item", {uid: uid, name: newName.value}, function () {
    $('#renameModal').modal('hide');
    getFiles();
    newName.value = "";
  });
}

// ****** Move Button ************
var moveButton = document.getElementById("moveButton");
moveButton.onclick = function() {
  var selected = getSelected();
  if (selected.length == 0) return;
  $('#moveModal').modal('show');
}

var submitMove = document.getElementById("submitMoveItem");
submitMove.onclick = function() {
  var selectedItems = getSelected();
  selectedItems.forEach(function(item) {
    var uid = item.parentNode.parentNode.id;
    var newPath = document.getElementById("newPath");
    if (newPath.value.length == 0) return false;

    $.post("/move_item", {uid: uid, path: newPath.value}, function () {
      $('#moveModal').modal('hide');
      getFiles();
      newPath.value = "";
    });
  });
}

// ****** Delete Button ************
var deleteButton = document.getElementById('deleteButton');
deleteButton.onclick = function() {
  var selectedItems = getSelected();
  selectedItems.forEach(function(item) {
    var uid = item.parentNode.parentNode.id;
    $.post("/delete_item", {uid: uid}, getFiles);
  });
}

// ****** Download Button ************
var downloadButton = document.getElementById('downloadButton');
downloadButton.onclick = function(){
  var selectedItems = getSelected();
  var uids = [];
  selectedItems.forEach(function(item) {
    var uid = item.parentNode.parentNode.id;
    uids.push(uid);
  });

  var xhr = new XMLHttpRequest();
  var url = "/download_items?items=" + JSON.stringify({uids: uids});
  xhr.responseType = "blob";
  xhr.open('GET', url);
  xhr.onreadystatechange = function() {
    if (xhr.status == 200) saveAs(xhr.response, "archive.zip");
  }
  xhr.send();
}
