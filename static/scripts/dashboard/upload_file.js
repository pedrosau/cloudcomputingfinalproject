var submitUploadButton = document.getElementById('submitUploadFile');
submitUploadButton.onclick = function() {
  var fileInput = document.getElementById('newFile');
  if (fileInput.length == 0) return false;
  for(var i = 0; i < fileInput.files.length; i++ ){
    file = fileInput.files[i]
    sendFile(file)
  }
  return false;
}

sendFile = function(file){
  var formData = new FormData();
  var xhr = new XMLHttpRequest();
  //File size limit.
  if (file.size > 300000000) {
    return false;
  }
  formData.append('file', file);
  formData.append('fname', file.name);
  formData.append('fsize', file.size);
  formData.append('ftype', file.type);
  xhr.open('POST', '/upload_file', true);
  xhr.send(formData);
  xhr.onreadystatechange = function () {
    if(xhr.status === 200) {
      $('#uploadModal').modal('hide');
      getFiles();
      var fileInput = document.getElementById('newFile');
      fileInput.value = null;
    }
  };
}