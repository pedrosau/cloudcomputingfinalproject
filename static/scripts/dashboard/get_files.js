$(document).ready(getFiles);

var directoryInfo =  {
  name: "",
  parent: ""
}

function getFiles() {
  $.getJSON("/get_dir_files", function(jsonResult) {
    directoryInfo.name = jsonResult.name;
    directoryInfo.parent = jsonResult.parent;
    displayFiles(jsonResult.contents);
    setupRows();
  });
}

function displayFiles(contents) {
  var tableBody = $("#directoryTableBody");
  tableBody.empty();
  contents.forEach(function(item) {
    var row = makeRow(item.name, item.date, item.size, item.type, item.is_directory, item.uid);
    tableBody.append(row);
  });
  $("#path-header").text("Files: " + directoryInfo.name);
}

function setupRows() {
  $(".directory-row").on("click", function(event) {
    var directoryID = event.currentTarget.parentNode.id;
    $.post("/update_directory", {directory_uid: directoryID}, getFiles);
  });
}

function makeRow(name, date, size, type, isDirectory, uid) {
  var shortName = (name.length > 20 ? name.substring(0, 20) + "..." : name);
  var style = (isDirectory ? "style=\"color: #3479f6\" " : " ");
  var id  = "id=" + uid + " ";
  var rowClass = (isDirectory ? "class=\"directory-row\" " : " ");

  return (
      "<tr " + style + id +  ">" +
      "<td><input type=\"checkbox\" name=\"checkbox\"/></td>" +
      "<td " + rowClass + ">" + shortName + "</td>" +
      "<td " + rowClass + ">" + date + "</td>" +
      "<td " + rowClass + ">" + size + "</td>" +
      "<td " + rowClass + ">" + type + "</td>" +
      "</tr>"
    );

}