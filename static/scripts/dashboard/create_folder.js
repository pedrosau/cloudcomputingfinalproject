var submitFolderButton = document.getElementById('submitCreateFolder')
submitFolderButton.onclick = function() {
  var newDir = document.getElementById('newDir');
  if (newDir.value.length == 0) return 0;

  $.post("/create_folder", {name: newDir.value}, function () {
    $('#createFolderModal').modal('hide');
    getFiles();
    newDir.value = "";
  });

  return false;
}