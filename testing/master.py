#!/usr/bin/python

import os
import sys
import json
import time
import boto3
import threading

# global variable to store important lambda return data
user_accounts = []
initial_results = []
upload_results = []
small_moves_results = []

def initial_file_upload(username, password):
    # pay load string
    p_str = "{\"username\":\"" + username + "\", \"password\":\"" + password + "\" }"
    #sending request
    client = boto3.client('lambda')
    print("Sending Request...")
    response = client.invoke(
            FunctionName="intial_file_upload",
            InvocationType='RequestResponse',
            Payload=p_str
        )
    r = json.loads(response['Payload'].read())
    # storing results
    initial_results.append((r['login'], r['upload']))

def create_user(username, email, password): 
    # pay load string
    p_str = "{\"username\":\"" + username + "\", \"password\":\"" + password + "\", \"email\":\"" + email + "\"}"
    #sending request
    client = boto3.client('lambda')
    print("Sending Request...")
    response = client.invoke(
            FunctionName="create_users",
            InvocationType='RequestResponse',
            Payload=p_str
        )
    r = json.loads(response['Payload'].read())
    # storing results
    user_accounts.append((r['username'], r['time'], r['status']))

def file_uploads(username, password, file_key): 
    # pay load string
    p_str = "{\"username\":\"" + username + "\", \"password\":\"" + password + "\", \"key\":\"" + file_key + "\"}"
    #sending request
    client = boto3.client('lambda')
    print("Sending Request...")
    response = client.invoke(
            FunctionName="project_testing_python_version",
            InvocationType='RequestResponse',
            Payload=p_str
        )
    r = json.loads(response['Payload'].read())
    # storing results
    upload_results.append((r['login'], r['upload']))

def newdir_rename_move(username, password):
    # pay load string
    p_str = "{\"username\":\"" + username + "\", \"password\":\"" + password + "\" }"
    #sending request
    client = boto3.client('lambda')
    print("Sending Request...")
    response = client.invoke(
            FunctionName="newdir_rename_move",
            InvocationType='RequestResponse',
            Payload=p_str
        )
    r = json.loads(response['Payload'].read())
    # storing results
    small_moves_results.append((r['login_time'], r['newdir_time'], r['rename_time'], r['move_time']))


if __name__== "__main__":
    start_time = time.time()
    #if len(sys.argv) != 2:
        # ensuring program is used correctly
    #    print("usage:\n ./master.py file")
    #    sys.exit(1)
    #in_file = sys.argv[1]
    fname = "password.txt"
    f = open(fname)  
    combinations = list()
    for line in f:
        u_name_and_pswd = line.strip()
        u_email = u_name_and_pswd + "@test.edu"
        combinations.append((u_name_and_pswd, u_email))
    print(combinations)
    """threads = 500
    lim = len(combinations)
    i = 0
    if lim < threads:
        # adjusting number of threads if there are less than 500 comparisons
        threads = lim
    for x in range(threads):
        for x in range(threads):
            if i < lim:
                t = threading.Thread(target=create_user, args=(combinations[i][0], combinations[i][1], combinations[i][0]))
                t.daemon = True
                t.start()
                i += 1
        main_thread = threading.currentThread()
        for t in threading.enumerate():
            if t is main_thread:
                continue
            t.join() # waiting for threads to return
    """
    