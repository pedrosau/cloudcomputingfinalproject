import zipfile
import os

# COmpresses a directory in the server
def make_zip(path):
    zipf = zipfile.ZipFile(path + '.zip', "w")
    for dir_name, sub_dirs, files in os.walk(path):
        zipf.write(dir_name)
        for filename in files:
            zipf.write(os.path.join(dir_name, filename))
    zipf.close()
    return path + '.zip'