import os
import sys
import json
import uuid
import datetime
import zipfile
import shutil

import boto3
import botocore

import flask
from flask import request, Response, redirect, url_for, flash, jsonify, session, send_file, after_this_request
from flask_login import LoginManager, login_required, login_user, logout_user
from werkzeug.utils import secure_filename
from flask_cors import CORS, cross_origin

from usercontroller import UserController, User
from filesystemcontroller import FileSystemController, File, Directory
import utilities

# Setting up flask application
application = flask.Flask(__name__)
application.config.from_object(__name__)
application.secret_key = 'DQRa3mEXEEp6ETnAIzdc5dE5lutmpJNnYieDMlSw'
CORS(application)
# Configure login manager
login_manager = LoginManager()
login_manager.init_app(application)
login_manager.login_view = "login"

# Controller to interact with user info
userController = UserController()

# Controller to interact the filesystem
fileSystemController = FileSystemController()

#---------------HTML TEMPLATE HANDLERS---------------------------------

# Homepage / Sign In page
@application.route('/')
def welcome():
    if request.method == 'GET':
        return flask.render_template('index.html', flask_debug=application.debug)

# Dashboard. The user's drive is accessed here.
@application.route('/dashboard')
@login_required
def dashboard():
    return flask.render_template('dashboard.html', flask_debug=application.debug)

#---------------ACCOUNT-RELATED HANDLERS-------------------------------

# Creates a new account if the username is not in use
@application.route('/signup', methods=['POST'])
def signup():
    try:
        user_root_uid = str(uuid.uuid4())
        response = userController.create_user(request.form['newUsername'], request.form['newEmail'], request.form['newPassword'], user_root_uid)
        if response['HTTPStatusCode'] == 200:
            name = request.form['newUsername']
            fileSystemController.put_directory(Directory(
                name=name,
                date=str(datetime.datetime.now()),
                owner=request.form['newUsername'],
                uid=user_root_uid
            ))

            return Response("", status=200)
        else:
            return Response("", status=500)
    except Exception as e:
        return Response("", status=409)

# Logs in a user by setting their  username and root directory in the session object.
@application.route('/login', methods=['POST'])
def login():
    username = request.form['username']
    password = request.form['password']
    if userController.authenticate(username, password):
        login_user(User(username))
        session['username'] = username
        session['directory_uid'] = userController.get_user_root_uid(username)
        return redirect(url_for('dashboard'))
    else:
        return redirect(url_for('welcome'))

# Logs out by clearing the session object.
@application.route('/logout', methods=['POST'])
@login_required
def logout():
    session['username'] = ''
    session['directory_uid'] = ''
    logout_user()
    return redirect(url_for('welcome'))

@login_manager.user_loader
def load_user(username):
    return User(username)

@login_manager.unauthorized_handler
def redirect_to_login():
    return redirect(url_for('welcome'))

#-------------------CREATING FILES AND DIRECTORIES---------------------

# Uploads a file. If another file with the same name already exists, then it will be replaced.
@application.route('/upload_file', methods=['POST'])
def upload_file_server():
    # Get file info
    file = request.files['file']
    filename = secure_filename(file.filename)

    form = request.form
    uid = str(uuid.uuid4())
    f = File(name=filename, date=str(datetime.datetime.now()), \
        parent=session['directory_uid'], size=form['fsize'], file_type=form['ftype'],\
        owner=session['username'], uid=uid , file_object=file)

    existing_file = fileSystemController.item_exists_in_directory(f.name, f.parent)
    if not existing_file:
        fileSystemController.upload_file(f)
        return Response("", status=200)
    elif not existing_file['is_directory']:
        fileSystemController.delete_item(existing_file['uid'])
        fileSystemController.upload_file(f)
        return Response("", status=200)
    else:
        return Response("", status=409)

# Creates a new directory in the current directory being displayed. If another
# one with the same name exists then it will not be created.
@application.route('/create_folder', methods=['POST'])
def create_folder():
    form = request.form
    dirname = secure_filename(form['name'])
    uid = str(uuid.uuid4())
    d = Directory(name=dirname, date=str(datetime.datetime.now()),\
        owner=session['username'], uid=uid, parent=session['directory_uid'])

    existing_dir = fileSystemController.item_exists_in_directory(d.name, d.parent)
    if not existing_dir:
        fileSystemController.put_directory(d)
        return Response("", status=200)
    else:
        return Response("", status=409)

#-------------------DIRECTORY NAVIGATION----------------------------------

# Returns the information of all the files in the directory being displayed to
# the client in JSON format.
@application.route('/get_dir_files', methods=['GET'])
def get_files_in_dir():
    contents, directory = fileSystemController.get_directory_contents(session['directory_uid'])
    return jsonify({'contents': contents, 'name': directory.name, 'parent': directory.parent})

# Updates the id of the directory being displayed to the client. This is used when
# the client navigates through their drive.
@application.route("/update_directory", methods=['POST'])
def update_directory():
    directory = fileSystemController.get_directory(request.form['directory_uid'])
    if directory.owner == session['username']:
        session['directory_uid'] = directory.uid
        return Response("", status=200)
    else:
        return Response("", status=403)

#---------------------FILESYSTEM OPPERATIONS-------------------------

# Renames a directory or file. If another object with the same name exists
# in the directory then the item won't be renamed.
@application.route("/rename_item", methods=['POST'])
def rename_item():
    form = request.form
    new_name = secure_filename(form['name'])
    uid = form['uid']
    parent = session['directory_uid']
    existing_item = fileSystemController.item_exists_in_directory(new_name, parent)
    if not existing_item:
        fileSystemController.rename_item(uid, session['directory_uid'], new_name)
        return Response("", status=200)
    else:
        return Response("", status=403)

# Moves an item to the specified path. Converts the path from a string to the target
# directory uid by recursively crawling the path. If the target directory already contains
# an item with the same name as the one being moved, the server will not fulfill the request.
@application.route("/move_item", methods=['POST'])
def move_item():
    form = request.form
    item_uid = form['uid']
    path = form['path'].split('/')
    path = path[:-1] if path[-1] == '' else path
    if path[0] == '':
        root_uid = userController.get_user_root_uid(session['username'])
        path = path[1:]
    else:
        root_uid = session['directory_uid']

    path_uids = fileSystemController.crawl_path(root_uid, path)
    target_dir_uid = path_uids[-1]

    if path_uids and not item_uid in path_uids:
        name = fileSystemController.get_item(item_uid).name
        existing_item = fileSystemController.item_exists_in_directory(name, target_dir_uid)

        if not existing_item:
            fileSystemController.change_parent(item_uid, session['directory_uid'], target_dir_uid)
            return Response("", 200)
        else:
            return Response("", status=403)
    else:
        return Response("", status=400)
    return Response("", 200)

# Deletes an item. If the item is a directoey then all of its contents are deleted.
@application.route("/delete_item", methods=['POST'])
def delete_item():
    #delete items recursively if its a directory
    form = request.form
    item_uid = form['uid']
    # check if it's a directory
    response = fileSystemController.get_item(item_uid)
    if response.is_directory == True:
        fileSystemController.handle_directory_delete(item_uid)
    else:
        fileSystemController.delete_item(item_uid)
    return Response("", 200)

# Sends all the selected items to the client. These will be downloaded from S3
# to the server, compressed and sent.
@application.route("/download_items", methods=['GET'])
def download_items():
    uids = json.loads(request.args.get('items'))['uids']
    foldername = '{}-{}'.format(session['username'], str(uuid.uuid4()))
    path = os.path.join('/tmp/', foldername)
    os.mkdir(path)
    fileSystemController.download_items(uids, path)
    zip_path = utilities.make_zip(path)
    shutil.rmtree(path)
    @after_this_request
    def delete_zip(response):
        try:
            os.remove(zip_path)
        except:
            pass
        return response
    return send_file(zip_path, mimetype="application/zip", as_attachment=True)

# Used for testing purposes.
@application.route("/download_items_direct", methods=['GET'])
@cross_origin()
def download_items_direct():
    data = json.loads(request.args.get('items'))
    uids = data['uids']
    username = data['username']
    foldername = '{}-{}'.format(username, str(uuid.uuid4()))
    path = os.path.join('/tmp/', foldername)
    os.mkdir(path)
    fileSystemController.download_items(uids, path)
    zip_path = utilities.make_zip(path)
    shutil.rmtree(path)
    @after_this_request
    def delete_zip(response):
        try:
            os.remove(zip_path)
        except:
            pass
        return response
    return send_file(zip_path, mimetype="application/zip", as_attachment=True)

if __name__ == '__main__':
    application.run(threaded=True)
